#include "Game.h"

#include <iostream>
#include <windows.h>

//-------------------------------------------------------------------------------------------------------

int main(int, char**)
{
	try
	{
		CGame * newGame;
		newGame = new CGame();
		newGame->Run();
		delete newGame;
	}
	catch(Panda::panda_runtime_error & e)
	{
		std::cerr << e.what() << std::endl;
		MessageBox(NULL, e.what(), "Panda Runtime Error", MB_OK | MB_ICONERROR | MB_TASKMODAL);
	}
	catch (...)
	{
		MessageBox(NULL, "Panda Unknown Error", "Panda Unknown Error", MB_OK | MB_ICONERROR | MB_TASKMODAL);
	}

	return 0;
}

//-------------------------------------------------------------------------------------------------------