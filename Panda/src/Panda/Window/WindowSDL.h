#ifndef WINDOWSDL_H
#define WINDOWSDL_H

#include "defines.h"

#include <SDL.h>
#include <string>

//-------------------------------------------------------------------------------------------------------

namespace Panda
{
	class PANDA_API CWindowSDL
	{
	public:
		CWindowSDL(const std::string& aName, int iWidth, int iHeight);
		~CWindowSDL();
		bool PollEvent(SDL_Event & aEvent);

	private:
		bool Init(const std::string& aName, int iWidth, int iHeight);

	private:
		SDL_Window * m_Window = nullptr;
	};
}

//-------------------------------------------------------------------------------------------------------

#endif // WINDOWSDL_H
