#include "WindowSDL.h"

#include "logic\PandaError.h"

//#include <stdexcept>

//-------------------------------------------------------------------------------------------------------

namespace Panda
{
	CWindowSDL::CWindowSDL(const std::string& aName, int iWidth, int iHeight)
	{
		if (!Init(aName, iWidth, iHeight))
		{
			#if defined(PANDA_DEBUG) ||  defined(PANDA_TESTING)
				throw panda_runtime_error(std::string("ERROR: Could not initialize SDL2"), __FILE__, __LINE__);
			#else
				throw panda_runtime_error("ERROR: Could not initialize SDL2!");
			#endif
		}
	}

	//-------------------------------------------------------------------------------------------------------

	CWindowSDL::~CWindowSDL()
	{
		SDL_DestroyWindow(m_Window);
		SDL_Quit();
	}

	//-------------------------------------------------------------------------------------------------------

	bool CWindowSDL::Init(const std::string& aName, int iWidth, int iHeight)
	{
		if (SDL_Init(SDL_INIT_VIDEO) != 0)
		{
			return false;
		}
		/* Request opengl 3.2 context.
		* SDL doesn't have the ability to choose which profile at this time of writing,
		* but it should default to the core profile */
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

		/* Turn on double buffering with a 24bit Z buffer.
		* You may need to change this to 16 or 32 for your system */
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

		m_Window = SDL_CreateWindow(aName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
			iWidth, iHeight, SDL_WINDOW_OPENGL);
		if (m_Window == nullptr)
		{
			return false;
		}

		return true;
	}

	//-------------------------------------------------------------------------------------------------------

	bool CWindowSDL::PollEvent(SDL_Event & aEvent)
	{
		return SDL_PollEvent(&aEvent);
	}
}

//-------------------------------------------------------------------------------------------------------