#include "PandaError.h"
#include "WindowGLFW.h"

#include <GLFW\glfw3.h>

//-------------------------------------------------------------------------------------------------------

namespace Panda
{
	CWindowGLFW::CWindowGLFW(const std::string& aName, int iWidth, int iHeight)
	{
		if (!Init(aName, iWidth, iHeight))
		{
#if defined(PANDA_DEBUG) ||  defined(PANDA_TESTING)
			throw panda_runtime_error(std::string("ERROR: Could not initialize SDL2"), __FILE__, __LINE__);
#else
			throw panda_runtime_error("ERROR: Could not initialize SDL2!");
#endif
		}
	}

	//-------------------------------------------------------------------------------------------------------

	CWindowGLFW::~CWindowGLFW()
	{
		glfwDestroyWindow(m_Window);
		glfwTerminate();
	}

	//-------------------------------------------------------------------------------------------------------

	bool CWindowGLFW::Init(const std::string& aName, int iWidth, int iHeight)
	{
		if (!glfwInit())
		{
			return false;
		}
		/* Request opengl 3.2 context.*/
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_DOUBLEBUFFER, GL_TRUE);

		glfwWindowHint(GLFW_SAMPLES, 0);
		glfwWindowHint(GLFW_RED_BITS, 8);
		glfwWindowHint(GLFW_GREEN_BITS, 8);
		glfwWindowHint(GLFW_BLUE_BITS, 8);
		glfwWindowHint(GLFW_ALPHA_BITS, 8);
		glfwWindowHint(GLFW_STENCIL_BITS, 8);
		glfwWindowHint(GLFW_DEPTH_BITS, 24);
		glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

		m_Window = glfwCreateWindow(iWidth, iHeight, aName.c_str(), NULL, NULL);
		if (m_Window == nullptr)
		{
			return false;
		}
		glfwMakeContextCurrent(m_Window);
		glfwSwapInterval(0);

		return true;
	}

	//-------------------------------------------------------------------------------------------------------

	void CWindowGLFW::PollEvent()
	{
		glfwPollEvents();
	}
}

//-------------------------------------------------------------------------------------------------------