#ifndef WINDOWGLFW_H
#define WINDOWGLFW_H

#include "defines.h"

#include <string>

//-------------------------------------------------------------------------------------------------------

struct GLFWwindow;

//-------------------------------------------------------------------------------------------------------

namespace Panda
{
	class PANDA_API CWindowGLFW
	{
	public:
		CWindowGLFW(const std::string& aName, int iWidth, int iHeight);
		~CWindowGLFW();
		void PollEvent();

		GLFWwindow * GetWindowPointer() const { return m_Window; }

	private:
		bool Init(const std::string& aName, int iWidth, int iHeight);

	private:
		GLFWwindow * m_Window = nullptr;
	};
}

//-------------------------------------------------------------------------------------------------------

#endif // WINDOWGLFW_H
