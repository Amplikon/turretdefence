#ifndef DEFINED_H
#define DEFINED_H

//-------------------------------------------------------------------------------------------------------

constexpr int GL_VERSION_MAJOR = 3;
constexpr int GL_VERSION_MINOR = 3;

#ifdef PANDA_BUILD_DYNAMIC //copy $(SolutionDir)bin\$(Platform)-$(Configuration)\$(ProjectName)\$(ProjectName).dll $(SolutionDir)bin\$(Platform)-$(Configuration)\$(SolutionName)
#ifdef PANDA_BUILD
	#define PANDA_API __declspec(dllexport)
#else
	#define PANDA_API __declspec(dllimport)
#endif
#else 
	#define PANDA_API	
#endif

//-------------------------------------------------------------------------------------------------------

#ifdef PANDA_DEBUG
	#ifdef NDEBUG
		#error Defining PANDA_DEBUG conflicts with NDEBUG
	#endif
#else
	#ifndef NDEBUG
		#error If PANDA_DEBUG is not defined, then NDEBUG should be defined for assert macro.
	#endif
#endif

//-------------------------------------------------------------------------------------------------------

#endif // DEFINED_H

