#ifndef TEMP_PTR_H
#define TEMP_PTR_H

#include "Defines.h"

//-------------------------------------------------------------------------------------------------------

namespace Panda
{	
	/**
	* Very trivial class which wraps pointer to object.
	*
	* This wrapper explicitly states that it's not managing lifetime of memory which is pointing to.
	* Object of this class is not usable if it's pointer is not initialized (default constructor does not initialie this pointer)
	* or assigned some value.
	* 
	*/
	template<typename T>
	class PANDA_API Temp_Ptr
	{
	public:
		T * m_Ptr;

	public:
		Temp_Ptr() = default; // : m_Ptr(nullptr) {}
		Temp_Ptr(T * Ptr) : m_Ptr(Ptr)
		{
		}
		void SetPtr(T * ptr)
		{
			m_Ptr = ptr;
		}
		T* operator->() {
			return m_Ptr;
		};
		explicit operator bool() const
		{
			return Ptr;
		}
	};
}

//-------------------------------------------------------------------------------------------------------

#endif // TEMP_PTR_H
