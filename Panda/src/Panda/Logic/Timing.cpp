#include "Timing.h"

//-------------------------------------------------------------------------------------------------------

namespace Panda
{
	std::chrono::time_point<std::chrono::steady_clock> PerformanceClock::m_GlobalClock;
	PandaNanoSeconds PerformanceClock::m_StaticElapsedDuration;
}

//-------------------------------------------------------------------------------------------------------