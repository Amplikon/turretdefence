#ifndef PANDA_ERROR
#define PANDAERROR_H

#include <stdexcept>
#include <string>

#include "Defines.h"

#if defined(PANDA_DEBUG) ||  defined(PANDA_TESTING)
	#define PANDA_RUNTIME_ERROR(string) panda_runtime_error(string, __FILE__, __LINE__)
#else
	#define PANDA_RUNTIME_ERROR(string) panda_runtime_error(string)
#endif

namespace Panda
{
	class PANDA_API panda_runtime_error : public std::runtime_error
	{
	public:
	#if defined(PANDA_DEBUG) ||  defined(PANDA_TESTING)
		panda_runtime_error(const std::string& aString, const std::string& aFile, int iLine) :
			std::runtime_error(aString + "\nOccured in file: " + aFile + " on line: " + std::to_string(iLine))
		{

		}
	#else
		panda_runtime_error(const std::string& aString) :
			std::runtime_error(aString)
		{

		}
	#endif
	};
}

#endif // PANDA_ERROR
