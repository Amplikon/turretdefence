#ifndef TIMING_H
#define TIMING_H

#include "Defines.h"

#include <chrono>

//-------------------------------------------------------------------------------------------------------

#if defined(PANDA_DEBUG) ||  defined(PANDA_TESTING)
	#define START_PERF_CLOCK(perfClock) perfClock.Start()
#else
	#define START_PERF_CLOCK(perfClock) ((void)0)
#endif

#if defined(PANDA_DEBUG) ||  defined(PANDA_TESTING)
	#define FINISH_PERF_CLOCK(perfClock) perfClock.Finish()
#else
	#define FINISH_PERF_CLOCK(perfClock) ((void)0)
#endif

namespace Panda
{
	using PandaSeconds = std::chrono::duration<double, std::ratio<1>>;
	using PandaNanoSeconds = std::chrono::nanoseconds;

	class Timing
	{
	public:
		static constexpr PandaSeconds UPDATE_TICK_DOUBLE = PandaSeconds(0.00390625);
		static constexpr PandaNanoSeconds UPDATE_TICK = PandaNanoSeconds(3906250);
	};

	class PerformanceClock
	{
	public:
		PerformanceClock()
		{
			m_ElapsedDuration = PandaNanoSeconds::zero();
		}

		void Start()
		{
			m_StartPoint = std::chrono::steady_clock::now();
		}

		void Finish()
		{
			auto endPoint = std::chrono::steady_clock::now();
			m_ElapsedDuration += (endPoint - m_StartPoint);
		}

		PandaNanoSeconds Reset()
		{
			PandaNanoSeconds elapsedDuration = m_ElapsedDuration;
			m_ElapsedDuration = PandaNanoSeconds::zero();
			return elapsedDuration;
		}

		static PandaNanoSeconds SynchronizeClocks()
		{
			auto globalEndPoint = std::chrono::steady_clock::now();
			PandaNanoSeconds elapsedDuration = globalEndPoint - m_GlobalClock;
			m_GlobalClock = globalEndPoint;
			return elapsedDuration;
		}

	private:
		std::chrono::time_point<std::chrono::steady_clock> m_StartPoint;
		PandaNanoSeconds m_ElapsedDuration;

		static std::chrono::time_point<std::chrono::steady_clock> m_GlobalClock;
		static PandaNanoSeconds m_StaticElapsedDuration;
	};
}

//-------------------------------------------------------------------------------------------------------

#endif // TIMING_H