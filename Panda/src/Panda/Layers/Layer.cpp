#include "Layer.h"

//-------------------------------------------------------------------------------------------------------

namespace Panda
{

	bool CLayer::OnCursorPosEvent(double x, double y)
	{
		return false;
	}

	//-------------------------------------------------------------------------------------------------------

	bool CLayer::OnMouseButtonEvent(int button, int action, int modifiers)
	{
		return false;
	}

	//-------------------------------------------------------------------------------------------------------

	bool CLayer::OnKeyEvent(int key, int scancode, int action, int mods)
	{
		return false;
	}

	//-------------------------------------------------------------------------------------------------------

	bool CLayer::OnCharEvent(unsigned int codepoint)
	{
		return false;
	}

	//-------------------------------------------------------------------------------------------------------

	bool CLayer::OnDropEvent(int count, const char **filenames)
	{
		return false;
	}

	//-------------------------------------------------------------------------------------------------------

	bool CLayer::OnScrollEvent(double x, double y)
	{
		return false;
	}

	//-------------------------------------------------------------------------------------------------------

	bool CLayer::OnResizeEvent(int width, int height)
	{
		return false;
	}
}

//-------------------------------------------------------------------------------------------------------