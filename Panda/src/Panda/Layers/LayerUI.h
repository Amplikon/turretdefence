#ifndef LAYERUI_H
#define LAYERUI_H

#include "Layer.h"

//-------------------------------------------------------------------------------------------------------

namespace nanogui
{
	class Screen;
}

struct GLFWwindow;

//-------------------------------------------------------------------------------------------------------

namespace Panda
{
	class PANDA_API CLayerUI : public CLayer
	{
	public:
		CLayerUI() = default;

		void Init(GLFWwindow * window);

		virtual void Update(int elapsedTicks) override;

		// Event handlers
		virtual bool OnCursorPosEvent(double x, double y) override;
		virtual bool OnMouseButtonEvent(int button, int action, int modifiers) override;
		virtual bool OnKeyEvent(int key, int scancode, int action, int mods) override;
		virtual bool OnCharEvent(unsigned int codepoint) override;
		virtual bool OnDropEvent(int count, const char **filenames) override;
		virtual bool OnScrollEvent(double x, double y) override;
		virtual bool OnResizeEvent(int width, int height) override;

	private:
		nanogui::Screen *screen = nullptr;
	};
}

//-------------------------------------------------------------------------------------------------------

#endif // MENULAYER_H