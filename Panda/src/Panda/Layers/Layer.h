#ifndef LAYER_H
#define LAYER_H

#include <chrono>

#include "Timing.h"
#include "Defines.h"

//-------------------------------------------------------------------------------------------------------

namespace Panda
{
	class PANDA_API CLayer
	{
	public:
		CLayer() = default;

		/**
		* Updates layer based on passedTime
		*
		* @param aTimePassed Passed time in seconds, since last update call.
		*
		*/
		virtual void Update(int elapsedTicks) = 0;

		// Event handlers
		virtual bool OnCursorPosEvent(double x, double y);
		virtual bool OnMouseButtonEvent(int button, int action, int modifiers);
		virtual bool OnKeyEvent(int key, int scancode, int action, int mods);
		virtual bool OnCharEvent(unsigned int codepoint);
		virtual bool OnDropEvent(int count, const char **filenames);
		virtual bool OnScrollEvent(double x, double y);
		virtual bool OnResizeEvent(int width, int height);
		
	private:
	};
}

//-------------------------------------------------------------------------------------------------------

#endif // LAYER_H