#include "LayerUI.h"
#include "Logger\Logger.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "nanogui\nanogui.h"

//-------------------------------------------------------------------------------------------------------

enum test_enum {
	Item1 = 0,
	Item2,
	Item3
};

bool bvar = true;
int ivar = 12345678;
double dvar = 3.1415926;
float fvar = (float)dvar;
std::string strval = "A string";
test_enum enumval = Item2;
//Color colval(0.5f, 0.5f, 0.7f, 1.f);

using namespace nanogui;

namespace Panda
{
	void CLayerUI::Init(GLFWwindow * window)
	{
		screen = new Screen();
		screen->initialize(window, true);

		Widget *panel = new Widget(screen);
		panel->setLayout(new BoxLayout(Orientation::Horizontal, Alignment::Middle, 0, 20));

		Button *b = new Button(panel, "Plain button");
		b->setCallback([] { PANDA_INFO("Button pushed"); });

		// Create nanogui gui
		//bool enabled = true;
		//FormHelper *gui = new FormHelper(screen);
		//ref<Window> nanoguiWindow = gui->addWindow(Eigen::Vector2i(10, 10), "Form helper example");
		//gui->addGroup("Basic types");
		//gui->addVariable("bool", bvar)->setTooltip("Test tooltip.");
		//gui->addVariable("string", strval);

		//gui->addGroup("Validating fields");
		//gui->addVariable("int", ivar)->setSpinnable(true);
		//gui->addVariable("float", fvar)->setTooltip("Test.");
		//gui->addVariable("double", dvar)->setSpinnable(true);

		//gui->addGroup("Complex types");
		//gui->addVariable("Enumeration", enumval, enabled)->setItems({ "Item 1", "Item 2", "Item 3" });
		//gui->addVariable("Color", colval)->setFinalCallback([](const Color &c) {
		//	std::cout << "ColorPicker Final Callback: ["
		//		<< c.r() << ", "
		//		<< c.g() << ", "
		//		<< c.b() << ", "
		//		<< c.w() << "]" << std::endl;
		//});

		//gui->addGroup("Other widgets");
		//gui->addButton("A button", []() { std::cout << "Button pressed." << std::endl; })->setTooltip("Testing a much longer tooltip, that will wrap around to new lines multiple times.");;

		screen->setVisible(true);
		screen->performLayout();
		//nanoguiWindow->center();
	}

	//-------------------------------------------------------------------------------------------------------

	void CLayerUI::Update(int elapsedTicks)
	{
		// Draw nanogui
		assert(screen);
		screen->drawContents();
		screen->drawWidgets();
	}

	//-------------------------------------------------------------------------------------------------------

	bool CLayerUI::OnCursorPosEvent(double x, double y)
	{
		return screen->cursorPosCallbackEvent(x, y);
	}

	//-------------------------------------------------------------------------------------------------------

	bool CLayerUI::OnMouseButtonEvent(int button, int action, int modifiers)
	{
		return screen->mouseButtonCallbackEvent(button, action, modifiers);
	}

	//-------------------------------------------------------------------------------------------------------

	bool CLayerUI::OnKeyEvent(int key, int scancode, int action, int mods)
	{
		return screen->keyCallbackEvent(key, scancode, action, mods);
	}

	//-------------------------------------------------------------------------------------------------------

	bool CLayerUI::OnCharEvent(unsigned int codepoint)
	{
		return screen->charCallbackEvent(codepoint);
	}

	//-------------------------------------------------------------------------------------------------------

	bool CLayerUI::OnDropEvent(int count, const char **filenames)
	{
		return screen->dropCallbackEvent(count, filenames);
	}

	//-------------------------------------------------------------------------------------------------------

	bool CLayerUI::OnScrollEvent(double x, double y)
	{
		return screen->scrollCallbackEvent(x, y);
	}

	//-------------------------------------------------------------------------------------------------------

	bool CLayerUI::OnResizeEvent(int width, int height)
	{
		return screen->resizeCallbackEvent(width, height);
	}

}

//-------------------------------------------------------------------------------------------------------