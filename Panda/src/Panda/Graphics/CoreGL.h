#ifndef COREGL_H
#define COREGL_H

#include "Defines.h"

//-------------------------------------------------------------------------------------------------------

struct GLFWwindow;

//-------------------------------------------------------------------------------------------------------

namespace Panda
{
	class PANDA_API CCoreGL
	{
	public:
		CCoreGL();
		~CCoreGL();

		void Init(GLFWwindow * window);
		void Render();

	private:
	};
}

//-------------------------------------------------------------------------------------------------------

#endif
