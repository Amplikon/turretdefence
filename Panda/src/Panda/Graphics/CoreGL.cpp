#include "PandaError.h"
#include "CoreGL.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

//-------------------------------------------------------------------------------------------------------

namespace Panda
{
	CCoreGL::CCoreGL()
	{
		/* Initialize the library */
	}

	//-------------------------------------------------------------------------------------------------------

	CCoreGL::~CCoreGL()
	{
	}

	//-------------------------------------------------------------------------------------------------------

	void CCoreGL::Init(GLFWwindow * window)
	{
		if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
			throw PANDA_RUNTIME_ERROR("Could not initialize GLAD!");
		glGetError(); // pull and ignore unhandled errors like GL_INVALID_ENUM

		int width, height;
		glfwGetFramebufferSize(window, &width, &height);
		glViewport(0, 0, width, height);
	}


	//-------------------------------------------------------------------------------------------------------

	void CCoreGL::Render()
	{
		/* Render here */
		glClear(GL_COLOR_BUFFER_BIT);
	}
}

//-------------------------------------------------------------------------------------------------------
