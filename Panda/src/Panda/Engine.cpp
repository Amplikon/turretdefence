#include "Engine.h"
#include "Defines.h"
#include "PlayingState.h"
#include "Logger\Logger.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <thread>
#include <iostream>


//-------------------------------------------------------------------------------------------------------

namespace Panda
{
	CEngine::CEngine() : m_Machine(*this, CGameState::GAMELOADING_STATE),
		m_Window("Test window", 800, 600)
	{
		Init();
	}

	//-------------------------------------------------------------------------------------------------------

	void CEngine::Init()
	{
		m_CoreGL.Init(m_Window.GetWindowPointer());
	}

	//-------------------------------------------------------------------------------------------------------

	void CEngine::Run()
	{
		m_LastTimePoint = std::chrono::steady_clock::now();
		PandaNanoSeconds elapsedFullDuration = PandaNanoSeconds::zero();
		int iElapsedTicks = 0;
#if defined(PANDA_DEBUG) ||  defined(PANDA_TESTING)
		PandaNanoSeconds elapsedFpsDuration = PandaNanoSeconds::zero();
		PerformanceClock::SynchronizeClocks();
#endif
		while (m_IsRunning)
		{
			START_PERF_CLOCK(m_PerfClock0);
			glClearColor(0.2f, 0.25f, 0.3f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT);
			FINISH_PERF_CLOCK(m_PerfClock0);

			START_PERF_CLOCK(m_PerfClock1);
			m_EventManager.HandleEvents();
			m_Machine.Update(iElapsedTicks);
			m_Machine.Purge();
			FINISH_PERF_CLOCK(m_PerfClock1);

			START_PERF_CLOCK(m_PerfClock2);
			glfwSwapBuffers(m_Window.GetWindowPointer());
			FINISH_PERF_CLOCK(m_PerfClock2);

			START_PERF_CLOCK(m_PerfClock3);
			// Timing
			auto aNewTimePoint = std::chrono::steady_clock::now();
			PandaNanoSeconds elapsedDuration = aNewTimePoint - m_LastTimePoint;
			elapsedFullDuration += elapsedDuration;
			iElapsedTicks = elapsedFullDuration / Timing::UPDATE_TICK;
			elapsedFullDuration -= (Timing::UPDATE_TICK*iElapsedTicks);
			m_LastTimePoint = aNewTimePoint;
			FINISH_PERF_CLOCK(m_PerfClock3);

#if defined(PANDA_DEBUG) ||  defined(PANDA_TESTING)
			elapsedFpsDuration += elapsedDuration;
			++m_NFrames;
			if (elapsedFpsDuration > PandaNanoSeconds(2'000'000'000))
			{
				PandaSeconds elapsedSeconds = elapsedFpsDuration;
				PandaSeconds clearTime = m_PerfClock0.Reset();
				PandaSeconds updateTime = m_PerfClock1.Reset();
				PandaSeconds drawTime = m_PerfClock2.Reset();
				PandaSeconds timingTime = m_PerfClock3.Reset();
				PandaSeconds totalTime = PerformanceClock::SynchronizeClocks();

				PANDA_INFO("Seconds per frame: {} Fps: {}", elapsedSeconds.count() / (float)m_NFrames, (float)m_NFrames / elapsedSeconds.count());
				PANDA_INFO("Seconds inside clear: {}", clearTime.count()/ totalTime.count());
				PANDA_INFO("Seconds inside update: {}", updateTime.count() / totalTime.count());
				PANDA_INFO("Seconds inside draw: {}", drawTime.count() / totalTime.count());
				PANDA_INFO("Seconds inside timing: {}\n", timingTime.count() / totalTime.count());
				elapsedFpsDuration -= PandaNanoSeconds(2'000'000'000);
				m_NFrames = 0;
			}
#endif
		}
	}
}

//-------------------------------------------------------------------------------------------------------