#ifndef ENGINE_H
#define ENGINE_H

//-------------------------------------------------------------------------------------------------------

#include "CoreGL.h"
#include "StaticStateMachine.h"
#include "WindowGLFW.h"
#include "Event.h"
#include "Timing.h"
#include "defines.h"


//-------------------------------------------------------------------------------------------------------

class GLFWindow;

//-------------------------------------------------------------------------------------------------------

namespace Panda
{

	class PANDA_API CEngine
	{
	public:
		CEngine();

		void Run();

		template<typename... Ts>
		void ChangeState(CGameState::State eState, Ts&&... args)
		{
			m_Machine.ChangeState(eState, std::forward<Ts>(args)...);
		}

		void SetStateEventCallbacks(CGameState * state)
		{
			m_EventManager.SetStateEventCallbacks(*state, m_Window);
		}
		
		GLFWwindow* GetWindow() const { return m_Window.GetWindowPointer(); }
		bool IsClosing() const { return m_IsRunning; }
		void CloseApp() { m_IsRunning = false; }

	private:
		void Init();

	private:
		CWindowGLFW m_Window;
		CCoreGL m_CoreGL;
		CStaticStateMachine m_Machine;
		CEventManager m_EventManager;
		std::chrono::time_point<std::chrono::steady_clock> m_LastTimePoint; ///< Last time point
		PerformanceClock m_PerfClock0;
		PerformanceClock m_PerfClock1;
		PerformanceClock m_PerfClock2;
		PerformanceClock m_PerfClock3;
		int m_NFrames = 0;
		bool m_IsRunning = true;

		int a = 0;
		int b = 0;
	};
}

//-------------------------------------------------------------------------------------------------------

#endif // ENGINE_H
