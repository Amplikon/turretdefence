#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "Temp_Ptr.h"
#include "Layer.h"

#include <chrono>
#include <memory>
#include <vector>

//-------------------------------------------------------------------------------------------------------

namespace Panda
{
	
	class PANDA_API CEngine;

	//-------------------------------------------------------------------------------------------------------

	class CGameState
	{
	public:
		/**
		* All possible static states
		*/
		enum State
		{
			NULL_STATE = -1,

			GAMELOADING_STATE = 0,
			MENU_STATE,
			PLAYING_STATE,

			STATE_COUNT
		};
	public:
		CGameState(CEngine & engine) : m_Engine(engine)
		{
		}
		virtual ~CGameState() noexcept {}

		void Init()
		{
		}
		
		virtual void Resume() = 0;
		virtual void Pause() = 0;

		virtual void Update(int elapsedTicks);

		// Event handlers
		virtual void OnCloseEvent();
		virtual void OnCursorPosEvent(double x, double y);
		virtual void OnMouseButtonEvent(int button, int action, int modifiers);
		virtual void OnKeyEvent(int key, int scancode, int action, int mods);
		virtual void OnCharEvent(unsigned int codepoint);
		virtual void OnDropEvent(int count, const char **filenames);
		virtual void OnScrollEvent(double x, double y);
		virtual void OnResizeEvent(int width, int height);


	protected:
		virtual void Start() = 0;
		virtual void Stop() = 0;

		CEngine& m_Engine;
		std::vector<std::unique_ptr<CLayer>> m_Layers;	///< Vector of all Layers in this state
		// TODO: Maybe Layers should be part of engine?? This could improve performance if same layer is used by two or more states.
	};
}

//-------------------------------------------------------------------------------------------------------

#endif // GAMESTATE_H
