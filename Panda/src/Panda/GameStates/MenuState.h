#ifndef MENUSTATE_H
#define MENUSTATE_H

#include "defines.h"
#include "GameState.h"

//-------------------------------------------------------------------------------------------------------

namespace Panda
{
	class PANDA_API CMenuState : public CGameState
	{
	public:
		CMenuState(CEngine& engine) : CGameState(engine) {}

		virtual void Resume() override;
		virtual void Pause() override;
		virtual void Update(int elapsedTicks) override;

	private:
		virtual void Start() override;
		virtual void Stop() override;
	};
}

//-------------------------------------------------------------------------------------------------------

#endif // MENUSTATE_H
