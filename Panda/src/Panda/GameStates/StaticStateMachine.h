#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include "defines.h"
#include "GameState.h"

#include <vector>
#include <memory>
#include <cassert>

//-------------------------------------------------------------------------------------------------------

/**
* States for this state machine are static which means that they are all created in state machine constructor and their lifespan should
* last as long as state machine exists.
*/
namespace Panda
{
	class CEngine;

	//-------------------------------------------------------------------------------------------------------

	class PANDA_API CStaticStateMachine
	{
	public:
		/**
		* Construct state machine adding first initial state
		*/
		CStaticStateMachine(CEngine& aEngine, CGameState::State eFirstState);
		/**
		* Add state to vector of states
		*/
		void CStaticStateMachine::AddState(CGameState::State eState);

		template<typename... Ts>
		void ChangeState(enum CGameState::State stateId, Ts&&... args)
		{
			assert(stateId > CGameState::NULL_STATE && stateId < CGameState::STATE_COUNT);
			PopState();
			PushState(stateId, std::forward<Ts>(args)...);
		}
		template<typename... Ts>
		void PushState(enum CGameState::State stateId, Ts&&... args)
		{
			assert(stateId > CGameState::NULL_STATE && stateId < CGameState::STATE_COUNT);;
			m_States[stateId]->Init(std::forward<Ts>(args)...);
			m_Waiting.push_back(stateId);
		}
		void PopState()
		{
			assert(m_StatesIndices.size() >= 1);
			m_Waiting.push_back(CGameState::NULL_STATE);
		}

		void Purge();

		void Update(int elapsedTicks)
		{
			CurrentState()->Update(elapsedTicks);
		}


	private:
		std::unique_ptr<CGameState> & CurrentState()
		{
			return m_States[m_StatesIndices.back()];
		}
	private:
		CEngine& m_Engine;
		std::vector<std::unique_ptr<CGameState>> m_States; ///< vector of all static states
		std::vector<int8_t> m_StatesIndices; ///< indices to all actual states
		std::vector<int8_t> m_Waiting;	///< states waiting for stop
		//uint8_t m_CurrentStateId;
	};
}

//-------------------------------------------------------------------------------------------------------

#endif // STATEMACHINE_H