#include "Engine.h"

#include "StaticStateMachine.h"

#include "GameLoadingState.h"
#include "MenuState.h"
#include "PlayingState.h"

//-------------------------------------------------------------------------------------------------------

namespace Panda
{
	CStaticStateMachine::CStaticStateMachine(CEngine& aEngine, CGameState::State eState) : m_Engine(aEngine)
	{
		m_States.reserve(CGameState::STATE_COUNT);

		for (int i = CGameState::NULL_STATE + 1; i < CGameState::STATE_COUNT; ++i)
		{
			AddState(static_cast<CGameState::State>(i));
		}
		assert(m_States.size() == CGameState::STATE_COUNT);

		m_StatesIndices.push_back(eState); // sets starting state
	}

	//-------------------------------------------------------------------------------------------------------

	void CStaticStateMachine::AddState(CGameState::State eState)
	{
		switch (eState)
		{
			case CGameState::GAMELOADING_STATE:
			{
				m_States.emplace_back(std::make_unique<CGameLoadingState>(m_Engine));
				break;
			}
			case CGameState::MENU_STATE:
			{
				m_States.emplace_back(std::make_unique<CMenuState>(m_Engine));
				break;
			}
			case CGameState::PLAYING_STATE:
			{
				m_States.emplace_back(std::make_unique<CPlayingState>(m_Engine));
				break;
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------

	void CStaticStateMachine::Purge()
	{
		for (auto id : m_Waiting)
		{
			if (id == CGameState::NULL_STATE)
			{
				m_States[m_StatesIndices.back()]->Pause();
				m_StatesIndices.pop_back();
			}
			else
			{
				m_States[id]->Resume();

				m_Engine.SetStateEventCallbacks(m_States[id].get());
				m_StatesIndices.push_back(id);
			}
		}
		m_Waiting.clear();
	}
}

//-------------------------------------------------------------------------------------------------------