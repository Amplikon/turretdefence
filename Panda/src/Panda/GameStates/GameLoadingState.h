#ifndef GAMELOADINGSTATE_H
#define GAMELOADINGSTATE_H

#include "defines.h"
#include "GameState.h"

//-------------------------------------------------------------------------------------------------------

namespace PANDA_API Panda
{
	class CGameLoadingState : public CGameState
	{
	public:
		CGameLoadingState(CEngine& engine) : CGameState(engine) {}

		virtual void Resume() override;
		virtual void Pause() override;
		virtual void Update(int elapsedTicks) override;

	private:
		virtual void Start() override;
		virtual void Stop() override;
	};
}

//-------------------------------------------------------------------------------------------------------

#endif // GAMELOADINGSTATE_H