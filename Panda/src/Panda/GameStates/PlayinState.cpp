#include "PlayingState.h"
#include "Engine.h"

//-------------------------------------------------------------------------------------------------------

namespace Panda
{
	CPlayingState::CPlayingState(CEngine& engine) : CGameState(engine)
	{
		std::unique_ptr<CLayerUI> aLayerUI = std::make_unique<CLayerUI>();
		aLayerUI->Init(engine.GetWindow());
		m_Layers.push_back(std::move(aLayerUI));
	}

	//-------------------------------------------------------------------------------------------------------

	void CPlayingState::Start()
	{
		//m_Engine.SetEventCallback(std::bind(&CPlayingState::OnEvent, this, std::placeholders::_1));
		//assert(m_LastTimePoint != std::chrono::steady_clock::time_point::min()); 
		m_Stopped = false;
	}

	//-------------------------------------------------------------------------------------------------------

	void CPlayingState::Stop()
	{
		m_Stopped = true;
	}

	//-------------------------------------------------------------------------------------------------------

	void CPlayingState::Resume()
	{
		if (m_Stopped)
			Start();
		else
		{
			m_Paused = false;
		}
	}

	//-------------------------------------------------------------------------------------------------------

	void CPlayingState::Pause()
	{
		if (m_Stopped)
			return;
		m_Paused = true;
	}

	//-------------------------------------------------------------------------------------------------------

	void CPlayingState::Update(int elapsedTicks)
	{
		//if (m_Stopped | m_Paused)
		//	return;

		for (auto & layer : m_Layers)
		{
			layer->Update(elapsedTicks);
		}

	}

	//-------------------------------------------------------------------------------------------------------

	void CPlayingState::OnCursorPosEvent(double x, double y)
	{
		for (auto & layer : m_Layers)
		{
			bool bHandled = layer->OnCursorPosEvent(x, y);
			if (bHandled)
				break;
		}
	}

	//-------------------------------------------------------------------------------------------------------

	void CPlayingState::OnMouseButtonEvent(int button, int action, int modifiers)
	{
		for (auto & layer : m_Layers)
		{
			bool bHandled = layer->OnMouseButtonEvent(button, action, modifiers);
			if (bHandled)
				break;
		}
	}

	//-------------------------------------------------------------------------------------------------------

	void CPlayingState::OnKeyEvent(int key, int scancode, int action, int mods)
	{
		for (auto & layer : m_Layers)
		{
			bool bHandled = layer->OnKeyEvent(key, scancode, action, mods);
			if (bHandled)
				break;
		}
	}

	//-------------------------------------------------------------------------------------------------------

	void CPlayingState::OnCharEvent(unsigned int codepoint)
	{
		for (auto & layer : m_Layers)
		{
			bool bHandled = layer->OnCharEvent(codepoint);
			if (bHandled)
				break;
		}
	}

	//-------------------------------------------------------------------------------------------------------

	void CPlayingState::OnDropEvent(int count, const char **filenames)
	{
		for (auto & layer : m_Layers)
		{
			bool bHandled = layer->OnDropEvent(count, filenames);
			if (bHandled)
				break;
		}
	}

	//-------------------------------------------------------------------------------------------------------

	void CPlayingState::OnScrollEvent(double x, double y)
	{
		for (auto & layer : m_Layers)
		{
			bool bHandled = layer->OnScrollEvent(x, y);
			if (bHandled)
				break;
		}
	}

	//-------------------------------------------------------------------------------------------------------

	void CPlayingState::OnResizeEvent(int width, int height)
	{
		for (auto & layer : m_Layers)
		{
			bool bHandled = layer->OnResizeEvent(width, height);
			if (bHandled)
				break;
		}
	}
}

//-------------------------------------------------------------------------------------------------------