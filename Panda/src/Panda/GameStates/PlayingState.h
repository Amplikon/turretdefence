#ifndef PLAYINGSTATE_H
#define PLAYINGSTATE_H

#include "defines.h"
#include "GameState.h"
#include "LayerUI.h"
#include "Event.h"

//-------------------------------------------------------------------------------------------------------

namespace Panda
{
	class PANDA_API CPlayingState : public CGameState
	{
	public:
		CPlayingState(CEngine& engine);

		virtual void Resume() override;
		virtual void Pause() override;
		virtual void Update(int elapsedTicks) override;

		// Event handlers
		virtual void OnCursorPosEvent(double x, double y) override;
		virtual void OnMouseButtonEvent(int button, int action, int modifiers) override;
		virtual void OnKeyEvent(int key, int scancode, int action, int mods) override;
		virtual void OnCharEvent(unsigned int codepoint) override;
		virtual void OnDropEvent(int count, const char **filenames) override;
		virtual void OnScrollEvent(double x, double y) override;
		virtual void OnResizeEvent(int width, int height) override;

	private:
		virtual void Start() override;
		virtual void Stop() override;

		bool m_Stopped = true; ///< Indicates that this state is stopped
		bool m_Paused = false; ///< Indicates that this state is paused
	};
}

//-------------------------------------------------------------------------------------------------------

#endif // PLAYINGSTATE_H