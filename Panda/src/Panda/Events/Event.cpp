#include "Event.h"
#include "Engine.h"

#include <GLFW/glfw3.h>

//-------------------------------------------------------------------------------------------------------

namespace Panda
{
	void CEventManager::HandleEvents()
	{
		glfwPollEvents();
	}

	//-------------------------------------------------------------------------------------------------------

	void CEventManager::SetStateEventCallbacks(CGameState & state, CWindowGLFW & window)
	{
		glfwSetWindowUserPointer(window.GetWindowPointer(), &state);

		glfwSetWindowCloseCallback(window.GetWindowPointer(),
			[](GLFWwindow * aWindow)
		{
			CGameState * state = reinterpret_cast<CGameState*>(glfwGetWindowUserPointer(aWindow));
			state->OnCloseEvent();
		});

		glfwSetCursorPosCallback(window.GetWindowPointer(),
			[](GLFWwindow * aWindow, double x, double y) {
			CGameState * state = reinterpret_cast<CGameState*>(glfwGetWindowUserPointer(aWindow));
			state->OnCursorPosEvent(x, y);
		});

		glfwSetMouseButtonCallback(window.GetWindowPointer(),
			[](GLFWwindow * aWindow, int button, int action, int modifiers) {
			CGameState * state = reinterpret_cast<CGameState*>(glfwGetWindowUserPointer(aWindow));
			state->OnMouseButtonEvent(button, action, modifiers);
		}
		);

		glfwSetKeyCallback(window.GetWindowPointer(),
			[](GLFWwindow * aWindow, int key, int scancode, int action, int mods) {
			CGameState * state = reinterpret_cast<CGameState*>(glfwGetWindowUserPointer(aWindow));
			state->OnKeyEvent(key, scancode, action, mods);
		}
		);

		glfwSetCharCallback(window.GetWindowPointer(),
			[](GLFWwindow * aWindow, unsigned int codepoint) {
			CGameState * state = reinterpret_cast<CGameState*>(glfwGetWindowUserPointer(aWindow));
			state->OnCharEvent(codepoint);
		}
		);

		glfwSetDropCallback(window.GetWindowPointer(),
			[](GLFWwindow * aWindow, int count, const char **filenames) {
			CGameState * state = reinterpret_cast<CGameState*>(glfwGetWindowUserPointer(aWindow));
			state->OnDropEvent(count, filenames);
		}
		);

		glfwSetScrollCallback(window.GetWindowPointer(),
			[](GLFWwindow * aWindow, double x, double y) {
			CGameState * state = reinterpret_cast<CGameState*>(glfwGetWindowUserPointer(aWindow));
			state->OnScrollEvent(x, y);
		}
		);

		glfwSetFramebufferSizeCallback(window.GetWindowPointer(),
			[](GLFWwindow * aWindow, int width, int height) {
			CGameState * state = reinterpret_cast<CGameState*>(glfwGetWindowUserPointer(aWindow));
			state->OnResizeEvent(width, height);
		}
		);
	}
}

//-------------------------------------------------------------------------------------------------------