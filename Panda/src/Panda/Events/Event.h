#ifndef EVENT_H
#define EVENT_H

#include <functional>
#include <cassert>

#include "defines.h"

//-------------------------------------------------------------------------------------------------------

namespace Panda
{
	class CEngine;
	class CWindowGLFW;
	class CGameState;

	//-------------------------------------------------------------------------------------------------------

	class PANDA_API CEventManager
	{
	public:

		CEventManager() = default;

		void HandleEvents();

		void SetStateEventCallbacks(CGameState & state, CWindowGLFW & window);
	};

	//class PANDA_API CEventManager
	//{
	//public:
	//	using EventCallback = std::function<void(SDL_Event & event)>;

	//	CEventManager() = default;
	//	CEventManager(const EventCallback & aEventCallback) : m_EventCallback(aEventCallback)
	//	{
	//	}
	//	void SetCallback(const EventCallback & aEventCallback)
	//	{
	//		m_EventCallback = aEventCallback;
	//	}

	//	void HandleEvents()
	//	{
	//		if (!m_EventCallback)
	//			return;
	//		assert(m_EventCallback);
	//		SDL_Event aEvent;
	//		while (SDL_PollEvent(&aEvent))
	//		{
	//			m_EventCallback(aEvent);
	//		}
	//	}

	//private:
	//	EventCallback m_EventCallback = nullptr;
	//};
}

//-------------------------------------------------------------------------------------------------------

#endif // EVENT_H