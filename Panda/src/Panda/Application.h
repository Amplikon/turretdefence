#ifndef APPLICATION_H
#define APPLICATION_H

//-------------------------------------------------------------------------------------------------------

#include "Defines.h"
#include "Engine.h"

//-------------------------------------------------------------------------------------------------------

namespace Panda
{
	class PANDA_API CApplication
	{
	public:
		CApplication();

		void Run();

	protected:
		CEngine  m_Engine;
	};

}
//-------------------------------------------------------------------------------------------------------

#endif // APPLICATION_H
