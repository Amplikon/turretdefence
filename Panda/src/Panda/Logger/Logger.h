#ifndef LOGGER_H
#define LOGGER_H

#include "spdlog\spdlog.h"

#include "Defines.h"

namespace Panda
{


#ifdef PANDA_TESTING
	#define PANDA_INFO(...) spdlog::info(__VA_ARGS__);
#else
	#define PANDA_INFO()
#endif


}

#endif // LOGGER_H
